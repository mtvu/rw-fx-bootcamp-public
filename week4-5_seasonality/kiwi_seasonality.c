/* 
Kiwi seasonality
*/

// define environment and load rw_tools
#define ACCT_CCY "USD"
#define DENOM_CCY "USD"
#define ASSET_LIST "AssetsDWX-FX-G10"

#include <rw_tools.c>

function run()
{
	set(PLOTNOW);
  	StartDate = 2000;
  	EndDate = 2018;
  	BarPeriod = 1440;
	LookBack = 1;
	MaxLong = MaxShort = 1;
	assetList(strf("%s\\%s.csv", AL_PATH, ASSET_LIST)); 
	
	asset("NZD/USD");
		
	if(month() == 8)
	{
		enterShort();
	}
	if(month() == 12)
	{
		enterLong();
	}
 
}