/*
Local Day Seasonality

Plot intradaay seaonality of a variable using a user-defined timezone

*/
// define environment and load rw_tools
#define ACCT_CCY "USD"
#define DENOM_CCY "USD"
#define ASSET_LIST "AssetsDWX-FX-USD"

#include <rw_tools.c>
#include <profile.c>

// plots intraday seasonality in local time. based on include/profile.c plotDay
void plotLocalDay(var value, int type, int timezone)
{
	int periods = 2*24;
	checkLookBack(periods);
	int m30 = 2*lhour(timezone, 0) + minute(0)/30;
	if(m30 > periods) return;
	plotSeason(m30, lhour(timezone, 0), ldow(timezone, 0), value, type);
}

function run()
{
	set(PLOTNOW);
  	StartDate = 2009;
  	EndDate = 2018;
  	BarPeriod = 60;
	int tz = ET;
	BarZone = tz;
	assetList(strf("%s\\%s.csv", AL_PATH, ASSET_LIST));
	
	while(asset(loop("EUR/USD", "GBP/USD", "AUD/USD", "NZD/USD")))
	{
		vars Close = series(priceClose());
		vars Return = series((Close[0] - Close[1])/Close[1]);

		plotLocalDay(Return[0], 0, tz);
	}
	
}