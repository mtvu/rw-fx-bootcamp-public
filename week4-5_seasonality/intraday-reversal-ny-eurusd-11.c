/*
Intraday Reversal in New York TimeFrame
 
*/
 
// define environment and load rw_tools
#define ACCT_CCY "USD"
#define DENOM_CCY "USD"
#define ASSET_LIST "AssetsDWX-FX-USD"
 
#include <rw_tools.c>
 
function run()
{
	set(PLOTNOW+PARAMETERS);
	setf(PlotMode, PL_ALL+PL_FINE);	
	StartDate= 2009;
	EndDate = 2019;
	BarPeriod = 60;
	BarZone = ET;
	MaxLong = MaxShort = 1;
	MonteCarlo = 0;
	assetList(strf("%s\\%s.csv", AL_PATH, ASSET_LIST)); 
	
	// Times to enter trades in ET
	int start_hour = 11;
	int end_hour = 11;
	
	while(asset(loop("EUR/USD"))) //, "USD/JPY","GBP/USD",   "USD/CAD", "AUD/USD", "NZD/USD", "USD/CHF")))
	{
		Spread = Commission = Slippage = RollLong = RollShort = 0;
		 
		vars Price = series(priceClose());
		vars Trend = series(SMA(Price,5));
		 
		if(lhour(ET) >= start_hour && lhour(ET) <= end_hour) 
		{
			if(Price[0]<Trend[0])
			{
				enterLong();
			} 
			else if(Price[0]>Trend[0]) 
			{
				enterShort();
			}
		}
		
		if(lhour(ET) == 18 or (ldow(ET)==5 and lhour(ET)>=15)) //prevent holding over the weekend - optional
		{
			exitLong();
			exitShort();
		}
	}
 
}