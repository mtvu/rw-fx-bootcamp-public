#include <profile.c>

function run()
{
	set(PLOTNOW);
  	StartDate = 2009;
  	EndDate = 2018;
  	BarPeriod = 60;
  	LookBack = 150;
 
  	vars Close = series(priceClose());
  	vars Return = series((Close[0] - Close[1])/Close[1]);
  	
  	plotCorrelogram(Return[0], 24*5);  
}