/* NFP DRIFT

NFP data is released first Friday of the month at 8:30 ET

*/

function run()
{
	set(PLOTNOW);
	StartDate = 2009;
	EndDate = 2018;
	BarPeriod = 5;
    asset("EUR/USD");

	int StartSeason = 600; //time of day to commence season
	int EndSeason = 1100; //time of day to end season
	
	vars Closes = series(priceClose());
	vars Returns = series((Closes[0]-Closes[1])/Closes[1]);
	
	// set up cumulative returns series to track returns over the whole season
	vars CumReturns = series();
	CumReturns[0] = (Returns[0]+1)*(CumReturns[1]+1)-1;
	
	// at StartSeason, reset CumReturn to zero
	if(ldow(ET) == 5 and day() <= 7 and ltod(ET) == StartSeason)
	{
		CumReturns[0] = 0;
	}
	// plot histogram
	static int i;
	if(is(INITRUN)) i = 0;	
	if(ldow(ET) == 5 and day() <= 7 and between(ltod(ET), StartSeason, EndSeason))
	{
		plotBar("SD_CumRet", i, ltod(ET), CumReturns[0]*100, DEV|BARS|LBL2, LIGHTBLUE);
		plotBar("Avg_CumRet", i, ltod(ET), CumReturns[0]*100, AVG|BARS|LBL2, BLUE);
//		plotBar("SD (x4)", i, ltod(ET), Returns[0]*100/4, DEV|BARS|LBL2, LIGHTBLUE);
//		plotBar("Avg", i, ltod(ET), Returns[0]*100, AVG|BARS|LBL2, BLUE);
		i++;	
	}
	
	if(ltod(ET) > EndSeason) i = 0;	
}