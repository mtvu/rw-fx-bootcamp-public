/* AUTOCORRELATION OF A MEAN REVERTING SPREAD

*/

#include <profile.c>

function run()
{
	set(PLOTNOW);
  	StartDate = 2016;
  	EndDate = 2017;
  	BarPeriod = 1440;
  	LookBack = 201;
  	
  	// get ETF data
	string Name;
	vars Closes[2];
	int n = 0;
	while(Name = loop("GLD", "GDX"))
	{
		if(is(INITRUN)) assetHistory(Name,FROM_AV);
		asset(Name);
		Closes[n] = series(priceClose());
		n++;
	}
	
	var beta = 0.3;
	vars spread = series((Closes[0])[0] - beta*(Closes[1])[0]);
	
  	plotCorrelogram(spread[0], 150);
}