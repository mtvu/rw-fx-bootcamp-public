#include <profile.c>

function run()
{
	set(PLOTNOW);
  	StartDate = 2009;
  	EndDate = 2018;
  	BarPeriod = 1440;
  	LookBack = 300;
	
	// get ETF data
  	if(is(INITRUN)) assetHistory("SPY", FROM_AV);
  	asset("SPY");
 
  	vars Close = series(priceClose());
  	vars Return = series((Close[0] - Close[1])/Close[1]);
  	
  	plotYear(Return[0], 1);
}