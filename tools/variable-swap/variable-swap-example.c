/* 
Example of using the variable_swap.c header file in a simulation. 
Refer to detailed instructions in variable_swap.c
*/

#define VARIABLE_SWAP
#define ASSET_LIST "AssetsDWX-FX-G10"
#define ACCT_CCY "USD"

#include <rw_tools.c>

var BROKER_FEE = 0.;

function run()
{
	set(LOGFILE);
	LookBack = 0;
	StartDate = 20170101;
	NumYears = 1;
	// EndDate =  20181231;
	
	if(is(INITRUN))
	{
		assetList(ASSET_LIST);
		string Name;
		stridx("zero_hash");  // we need a hash for zero as we can't assign zero as a dataset handle. 
		while(Name = loop(Assets))
		{
			// set up unique hash values for each dataset
			if(assetType(Name) != FOREX)
				continue;
			stridx(base_currency());
			stridx(quote_currency());
		}
	}

	while(asset(loop(Assets)))
	{
		set_roll(BROKER_FEE);
		printf("\n%s: %.5f, %.5f", Asset, RollLong, RollShort);
	}
}


