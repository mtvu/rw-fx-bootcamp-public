---
title: "Week 3 Exploratory Analysis Exercise"
output:
  html_document:
    df_print: paged
---


```{r}
library(quantmod)
library(glue)
options(digits.secs=3)
```

## Load some EUR/USD tick data and plot

```{r}
# set up file paths to tick data downloaded from Darwinex
path <- "D:/ResilioSync/FXBootcamp"
ticks_file <- "EURUSD_2019.csv"

# read in data  
ticks <-  read.csv(file.path(path, ticks_file),
                  header = TRUE, sep = ",", 
                  stringsAsFactors = FALSE)


# convert timestamp to posix
ticks[, "Date"] <- as.POSIXct(ticks[, "Date"], tz="GMT")

# convert to xts
ticks <- as.xts(ticks[, 2:3], order.by = ticks$Date)

# fill forward to estimate "continuous" spread
ticks_ffwd <- na.locf(ticks)

# calculate "continuous" spread
spread <- ticks_ffwd$ASK - ticks_ffwd$BID
spread[spread<0] <- 0

# plot subset of raw bid asks
plot.xts(ticks['2019-04-05 00/2019-04-05 01'], type='s', main = 'Ticks')

# plot subset of estimated "continuous" bid-asks
plot.xts(ticks_ffwd['2019-04-05 00/2019-04-05 01'], type='s', main = 'Continuous Bid-Ask Estimate')

# plot subset of estimated "continuous" spread
plot.xts(spread['2019-04-05 00/2019-04-05 01'], type='s', main = 'Continuous Spread Estimate')
```

## Sunday Open 

```{r}
plot.xts(ticks['2019-03-31'], type='s', main='Ticks following Sunday Open')

plot.xts(spread['2019-03-31'], type='s', main='Estimate spread following Sunday Open')
```

Notice that the first hour of trading after the Sunday open is very sparse in terms of tick frequency. You can also see that the estimated spread is incredibly wide - up to approximately 8 ticks in the most liquid of all the pairs! Normally we'd expect to see a spread of 0.5-1.0 pips, and indeed things settle down to that sort of range as liquidity starts coming in and the tick frequency picks up. 

### Implications

Remember how Zorro, by default, assumes a constant spread width in it's simulations? You can set this up by script, otherwise Zorro takes it from the asset list. You might decide to be conservative and tell Zorro to assume 1.0 pip for this pair. But this would drastically underestimate the cost of trading a strategy that placed its orders within the first hour of trading. 

We've all been fooled at one point or another by this one! The main take-away is to always check your assumptions!

## London Open

```{r}
plot.xts(ticks['2019-04-01 06:55/2019-04-01 07:05'], type='s', main='Ticks around London Open')

ticks_pre_open <- length(ticks['2019-04-01 06:00/2019-04-01 06:59'])
ticks_post_open <- length(ticks['2019-04-01 07:00/2019-04-01 07:59'])

print(glue("There are {ticks_pre_open} total ticks from 6am-7am and {ticks_post_open} ticks from 7am-8am"))
```
If we plot the ticks in the 5 minutes before and after 7am, you can sort of make out an increase in their frequency in the latter 5 minutes. 

If we count the number of ticks in the hour before 7am and the hour after, we find that there are around 50% more ticks in the second hour. So we see quite a significant pick up in trading activity.

We can use tick frequency as something like a proxy for volume in an OTC product like spot FX. This isn't particularly helpful if we're interested in absolute volumes, but it can provide insight into relative volumes. 

## The New York Open

First, we do some date-time wrangling to get the time of the NY open in GMT.

Then, we plot the ticks in the 5 minutes before and after the NY open. We also count the number of ticks in the hour before and the hour after. 

```{r}
# get time of NY open in London
ny_open <- as.POSIXct("2019-04-01 07:00", tz="America/New_York")
ny_open_gmt <- ny_open
attributes(ny_open_gmt)$tzone <- "Europe/London"

# check out changes in tick frequency
plot.xts(ticks[paste0(ny_open_gmt - 5*60, "/", ny_open_gmt + 5*60)], type='s', main='Ticks around NY Open')
ticks_pre_open <- length(ticks[paste0(ny_open_gmt - 60*60, "/", ny_open_gmt - 1*60)])
ticks_post_open <- length(ticks[paste0(ny_open_gmt, "/", ny_open_gmt + 59*60)])

print(glue("There are {ticks_pre_open} total ticks from 6am-7am NY and {ticks_post_open} ticks from 7am-8am NY"))

```
This time we can see yet another approximately 50% increase in tick frequency in the hours pre and post NY open. 

We'll use Zorro to plot rolling 60-minute volatility of minutely returns, as well as some interesting intraday seasonal patterns - see the Zorro section of the walkthrough.  

## US Economic Data Releases

During this week, we had:
* Monday: retail sales data at 12:30pm GMT (came in slightly worse than consensus)  
* Tuesday: Durable goods orders data at 12:30pm GMT (mostly slightly worse than consensus)  
* Wednesday: Empoyment change at 12:15pm GMT (worse than expected)  
* Thursday: Inital jobless claims at 12:30pm GMT (better than expected)  
* Friday: Non-Farm Payrolls at 12:30pm GMT (better than expected)  

Let's plot each of these events:

```{r}

dates <- c("01", "02", "03", "04", "05")
for(d in dates) {
  timestamp <- as.POSIXct(paste0("2019-04-", d, "12:30"), tz="Europe/London")
  print(plot.xts(ticks[paste0(timestamp - 15*60, "/", timestamp + 15*60)], type='s', main='Ticks around NY Data Release'))
}


```

Some of the news events turn out to be "non-events", but you can also cearly see the decreased liquidity leading into some of these news releases, the brust of activity of the event itself, and then back to normal trading in the aftermath. 

Be careful of your spread assumptions leading into these events, as well as your slippage assumptions over the events themselves - when big moves like this happen in short periods of time, your probability of experiencing significant slippage goes up. 

## Next steps

We'll leave you to work through the remaining items, but do shout out on Slack with any questions. Also, we'd love to hear your insights regarding what you find. 

Note that for some of the work, you'll need to load a different instrument - USD/CAD

