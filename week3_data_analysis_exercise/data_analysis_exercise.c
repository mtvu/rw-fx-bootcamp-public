/*
Data analysis exercise

Zorro can work with tick data, but there are limitations with the free version (minimum 1-minute bar period)
If you have Zorro v2.12 or greater, you have the chart viewer as a free feature. 

Open your Zorro configuration file and set HistoryFolder = "D:\path-to-tick-data"

*/

function run()
{
	set(PLOTNOW);
	StartDate = 20190331; // Sunday 
	EndDate = 20190405;   // Friday 
	History = ".t1";
	BarPeriod = 1;
	
	asset("EUR/USD");
	vars closes = series(priceClose());
	
	// volatility of one-minute returns
	int stddev_period = 60;
	vars returns = series(ROCP(closes, 1));
	vars volatility = series(100*sqrt(Moment(returns, stddev_period, 2)));
	
	plot("volatility", volatility, NEW, BLUE);
	
	// useful time and date functions
	
	// plot dots above price bars for the first hour of London trading
	if(lhour(WET) == 7)
		plot("London Open", priceHigh(), MAIN|DOT, MAGENTA);
	
	// plot an orange line through the mean price of each bar from half an hour before to half an hour after the NY open
	if(between(ltod(ET), 630, 730))
		plot("NY Open", price(), MAIN, ORANGE);
	
	
}